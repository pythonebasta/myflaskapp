FROM python:3.8.3-alpine
#FROM ubuntu:16.04
#RUN apt-get update && apt-get install -y python python-pip
#RUN pip3 install --upgrade setuptools
#RUN pip3 install --upgrade pip
RUN pip install flask
COPY app.py /opt/
ENTRYPOINT FLASK_APP=/opt/app.py flask run --host=0.0.0.0 --port=5000

